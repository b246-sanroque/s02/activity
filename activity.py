# Activity:
# 1. Accept a year input from the user and determine if it is a leap year or not.
# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

# Stretch Goal:
# 1. Add a validation for the leap year input:
# - Strings are not allowed for inputs
# - No zero or negative values


year = int(input("Enter a year: \n"))
if year < 0:
	print(f"{year} is not a valid year.")
else:
	if year % 400 == 0:
		print(f"{year} is a leap year.")
	elif year % 100 == 0:
		print(f"{year} is not a leap year.")
	if year % 4 == 0:
		print(f"{year} is a leap year.")
	else: 
		print(f"{year} is not a leap year.")


row = int(input("Enter number of rows: \n"))
col = int(input("Enter number of columns: \n"))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()

